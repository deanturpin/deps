#!/bin/bash

for proj in libs/*; do
	git subtree push --prefix=$proj git@gitlab.com:deanturpin/$(basename $proj) master
done
