# References
- [C++17 on Wikipedia](https://en.wikipedia.org/wiki/C%2B%2B17)
- [Modern C++ features](https://github.com/AnthonyCalandra/modern-cpp-features)
- Guidelines by [Stroustrup and Sutter](https://github.com/isocpp/CppCoreGuidelines)
