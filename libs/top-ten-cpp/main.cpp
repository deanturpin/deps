#include <list>
#include <string>
#include <functional>
#include <future>
#include <iostream>

// There’s a lot to take in
// If nothing else do these things
int main()
{
	////////////////////////////////////////////////////////////
	// Initialise class members in the header
	struct S1
	{
		int a = 0;
	};

	////////////////////////////////////////////////////////////
	// Brace initialisers / Initialiser lists
	// These take a bit of getting used to but they do give you extra
	// checks. For example the compiler coughs a narrowing warning for the
	// following.  We used to create a vector and then push elements onto
	// it (ignoring the potential copy overhead of [resizing
	// vectors](https://deanturpin.gitlab.io/post/vector)). But with
	// initialiser lists you can populate containers much more concisely.

	const double wide{1.0};
	const float narrow{static_cast<float>(wide)};
	std::list v1{1, 2, 3, 4, 5, 6};
	const std::pair<int, std::string> p1{1, "two"};

	// Initialise more complex types
	const struct S2 {
		int x;
		struct Foo {
			int u;
			int v;
			int a[3];
		} b;
	} s1 = {1, {2, 3, {4, 5, 6}}};


	// Range-based for loops Clumsy explicit iterator declarations can be cleaned
	// up with `auto`. In fact we can drop the iterators altogether and avoid that
	// \*i dereferencing idiom.  Note you don't have access to the current index
	// (until C++2a). Which isn't necessarily a bad thing.

	// for (std::list::iterator i = v1.begin(); i != v1.end(); ++i)
	// 	*i += 1;

	// for (auto i = v1.begin(); i != v1.end(); ++i)
	// 	*i += 1;

	for (auto &i : v1)
		i += 1;

	// Lambda expressions
	// Think function pointers but a much friendlier
	// implementation. Call like a regular function or pass them as a parameter.
	// You can also define them in-place so you don't have to go hunting for the
	// implementation like you might if you passed a function name. Here's another
	// new for-loop variation too. Note the use of `std::cbegin()` rather than the
	// method.

	const auto printer = []{ std::cout << "I am a first-class citizen\n"; return; };

	// Call like a function
	printer();
	
	// In-place lambda definition
	const std::vector d{0.0, 0.1, 0.2};
	
	std::for_each(std::cbegin(d), std::cend(d),[](const auto &i) { std::cout << i << "\n"; });
	
	// Threads
	// Thread are much neater than the old POSIX library but futures are
	// really interesting and let you return the stuff you're interested in
	// much more easily.  Define a processor-heavy routine as a lambda.
	// Here the return has been declared explicitly.  And effectively push
	// our complicated routine into the background and get on with
	// something else. Note we don't need to define what `f` is thanks to
	// `auto`. (It's actually a `std::future`.) When we're ready we block
	// to get the value. We could change the return type of complicated()
	// and nothing else needs to change.
	
	const auto complicated = []() -> int { return 1; };
	// auto f = std::async(std::launch::async, complicated);
	// const auto f1 = f.get();
}
