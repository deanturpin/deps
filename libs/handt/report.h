#pragma once

#include "backtest.h"
#include "trade.h"
#include <string>
#include <vector>

std::string get_report(const std::vector<trade_t> &,
                       const std::vector<backtest_t> &, const unsigned int &,
                       const bool &);

