#pragma once

#include "backtest.h"
#include "trade.h"
#include <vector>

void get_detailed_report(const std::vector<trade_t> &,
                         const std::vector<backtest_t> &);

