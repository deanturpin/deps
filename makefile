# Compilers
CXX := clang++-10

# Cut down flags for projects that are expected to have warnings
CXXFLAGS_SIMPLE := --std=c++20 --all-warnings --extra-warnings

# gcc flags
GCCCXXFLAGS := $(CXXFLAGS_SIMPLE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wattribute-alias -Wformat-overflow -Wformat-truncation \
	-Wmissing-attributes -Wstringop-truncation \
	-Wdeprecated-copy -Wclass-conversion \
	-O1
	# Add compat warning

CLANGCXXFLAGS := $(CXXFLAGS_SIMPLE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy

CXXFLAGS := $(CLANGCXXFLAGS)

# Define to run package manager rule in each submodule
PACMAN=

# All projects are stored in libs
SUBDIRS := $(wildcard libs/*)

# This allows the calling script to change the way the repo is cloned
# GitLab uses HTTPS to avoid having to exchange ssh keys
AUTH ?= git@gitlab.com:deanturpin

all: $(PACMAN) $(SUBDIRS)

# Update and build each lib
$(SUBDIRS):
	@echo "\n### $@ ###\n"
	git subtree pull --prefix=$@ $(AUTH)/$(shell basename $@) master --squash
	$(MAKE) CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" --directory $@ $(PACMAN)

.PHONY: $(SUBDIRS)

push:
	./push-all-subtrees.sh

apt:
	apt update
	apt install --yes git clang-10

clean:
	@echo foreach dir yeah
